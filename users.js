
function fetchData() {

    const loader = document.querySelector("#loading");

    fetch('https://fakestoreapi.com/users')
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.status);
            } else {
                return response.json();
            }
        })
        .then(users => {
            if (Array.isArray(users) && users.length == 0) {
                return Promise.reject("No Users found")
            } else {
                loader.classList.remove("display");
                return userRender(users);
            }
        })
        .catch(err => {
            const usersSection = document.querySelector(".users");
            const errorDiv = document.createElement('p');
            errorDiv.setAttribute('id', 'error');
            errorDiv.textContent = err.message;

            loader.classList.remove("display");
            usersSection.appendChild(errorDiv);
        });
}


function userRender(users) {

    const usersDiv = document.querySelector("#users");

    users.forEach((user) => {

        const userCard = document.createElement("div");
        const userFullName = document.createElement("h2");
        const userEmail = document.createElement("p");
        const userAddress = document.createElement("p");
        const userContactNo = document.createElement("p");
        const nameSpan = document.createElement("span");
        const emailSpan = document.createElement("span");
        const addressSpan = document.createElement("span");
        const contactSpan = document.createElement("span");
        const nameText = user.name.firstname + " " + user.name.lastname;
        const addressText = [user.address.street, user.address.city, `Zipcode- ${user.address.zipcode}`].join(', ');

        userCard.setAttribute('class', 'user');
        userFullName.setAttribute('class', 'fullName');
        userEmail.setAttribute('class', 'email');
        userAddress.setAttribute('class', 'address');
        userContactNo.setAttribute('class', 'contact');

        nameSpan.textContent = "Name-\n";
        emailSpan.textContent = "Email-\n";
        addressSpan.textContent = "Address-\n";
        contactSpan.textContent = "Contact no-\n";

        userFullName.append(nameSpan, nameText);
        userAddress.append(addressSpan, addressText);
        userEmail.append(emailSpan, user.email);
        userContactNo.append(contactSpan, user.phone);
        userCard.appendChild(userFullName);
        userCard.appendChild(userEmail);
        userCard.appendChild(userContactNo);
        userCard.appendChild(userAddress);

        usersDiv.appendChild(userCard);
    });
}

fetchData();