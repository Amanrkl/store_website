
function fetchData() {

    const loader = document.querySelector("#loading");

    fetch('https://fakestoreapi.com/products')
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.status);
            } else {
                return response.json();
            }
        })
        .then(products => {
            if (Array.isArray(products) && products.length == 0) {
                return Promise.reject("No Products found")
            } else {
                loader.classList.remove("display");
                return productRender(products);
            }
        })
        .catch(err => {
            const productsSection = document.querySelector(".products");
            const errorDiv = document.createElement('p');
            errorDiv.setAttribute('id', 'error');
            errorDiv.textContent = err.message;

            loader.classList.remove("display");
            productsSection.appendChild(errorDiv);
        })
}


function productRender(products) {

    const productsDiv = document.querySelector("#products");

    products.forEach((product) => {

        const productCard = document.createElement("div");
        const imageholder = document.createElement("div");
        const productImage = document.createElement("img");
        const productTitle = document.createElement("h1");
        const productCategory = document.createElement("p");
        const productRating = document.createElement("p");
        const productPrice = document.createElement("p");
        const descriptionContainer = document.createElement("div");
        const details = document.createElement("h2");
        const productDescription = document.createElement("p");

        productCard.setAttribute('class', 'product');
        imageholder.setAttribute('class', 'product-imageholder');
        productImage.src = product.image;
        productImage.alt = 'image';
        productTitle.setAttribute('class', 'title');
        productCategory.setAttribute('class', 'category');
        productRating.setAttribute('class', 'rating');
        productPrice.setAttribute('class', 'price');
        descriptionContainer.setAttribute('class', 'product-description');
        productDescription.setAttribute('class', 'description');

        productTitle.textContent = product.title;
        productCategory.textContent = product.category;
        productRating.textContent = product.rating.rate + ' ' + `(${product.rating.count})`;
        productPrice.textContent = "$" + product.price;
        details.textContent = "Product Details";
        productDescription.textContent = product.description;

        imageholder.appendChild(productImage);
        descriptionContainer.appendChild(details);
        descriptionContainer.appendChild(productDescription);
        productCard.appendChild(imageholder);
        productCard.appendChild(productTitle);
        productCard.appendChild(productCategory);
        productCard.appendChild(productRating);
        productCard.appendChild(productPrice);
        productCard.appendChild(descriptionContainer);

        productsDiv.appendChild(productCard);
    });
}

fetchData();