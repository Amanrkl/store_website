const form = document.getElementById('myform');

form.addEventListener("submit", (event) => {
    event.preventDefault();
    console.log("submit button click")
    if (validatingSignUpForm()) {
        console.log("Sign up Sucessful");
        const sucessMessage = document.querySelector('.successMessage');
        sucessMessage.style.display = 'block';
        form.parentElement.style.display = "none";
    } else {
        console.log("Failed to signup")
    }
});


function validatingSignUpForm() {

    const firstName = document.getElementById('firstName');
    const lastName = document.getElementById('lastName');
    const email = document.getElementById('emailAddress');
    const password = document.getElementById('userPassword');
    const confirmPassword = document.getElementById('confirmPassword');
    const tosBox = document.getElementById('tos');

    const nameRegex = /^[a-zA-Z]{2,30}$/
    const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

    let hasError = false;

    if (firstName.value === "") {
        setErrorMessage(firstName, "First name field can't be blank. Required this field.");
        hasError = true;
    } else if (!nameRegex.test(firstName.value)) {
        setErrorMessage(firstName, "Please enter valid first name");
        hasError = true;
    } else {
        setSuccessMessage(firstName);
    }

    if (lastName.value === "") {
        setErrorMessage(lastName, "Last name field can't be blank. Required this field.");
        hasError = true;
    } else if (!nameRegex.test(lastName.value)) {
        setErrorMessage(lastName, "Please enter valid last name");
        hasError = true;
    } else {
        setSuccessMessage(lastName);
    }

    if (email.value === "") {
        setErrorMessage(email, "Email field can't be blank. Required this field.");
        hasError = true;
    } else if (!emailRegex.test(email.value)) {
        setErrorMessage(email, "Please enter valid email address");
        hasError = true;
    } else {
        setSuccessMessage(email);
    }

    if (password.value === "") {
        setErrorMessage(password, "Password field can't be blank. Required this field.");
        hasError = true;
    } else if (password.value.length < 6) {
        setErrorMessage(password, "This field minimum character is 6. Please input at this range.");
        hasError = true;
    } else {
        setSuccessMessage(password);
    }

    if (confirmPassword.value === "") {
        setErrorMessage(confirmPassword, "Password field can't be blank. Required this field.");
        hasError = true;
    } else if (confirmPassword.value !== password.value) {
        setErrorMessage(confirmPassword, "Sorry! Your define password and Retype password not match. Please input correct password.");
        hasError = true;
    } else {
        setSuccessMessage(confirmPassword);
    }

    if (tosBox.checked) {
        form.firstElementChild.textContent = "";
    } else {
        form.firstElementChild.textContent = "Please check to agree";
        hasError = true;
    }

    return !hasError;
}

function setErrorMessage(inputElement, errorMessage) {
    let messageElement;
    if (inputElement.nextElementSibling.className !== 'message') {
        messageElement = document.createElement('p');
        messageElement.classList.add('message');
        inputElement.after(messageElement);
    } else {
        messageElement = inputElement.nextElementSibling;
    }

    messageElement.textContent = errorMessage;
    inputElement.style.border = "2px solid red";
}

function setSuccessMessage(inputElement) {
    if (inputElement.nextElementSibling.className === 'message') {
        inputElement.nextElementSibling.remove()
    }
    inputElement.style.border = "2px solid green";
}

